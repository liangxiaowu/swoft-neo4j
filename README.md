# swoft-neo4j

#### 介绍
基于swoft的neo4j数据库操作组件

#### 安装
```txt
composer require wunder/neo4j
```

#### bean配置
```php
'neo4j'      => [
        'class'   =>\Wunder\Neo4j\Client::class,
        'scheme'  => "http",
        'host'    => '192.168.1.100',
        'port'    => 7474,
        'username'    => 'neo4j',
        'password'    => '123456',
    ],
    'neo4j.pool' => [
        'class'  => \Wunder\Neo4j\Pool::class,
        'client' => bean('neo4j'),
        'minActive'   => 10,
        'maxActive'   => 1000,
        'maxWait'     => 0,
        'maxWaitTime' => 5,
        'maxIdleTime' => 40,
    ]
```
neo4j
- class: 当前配置驱动类`\Wunder\Neo4j\Client::class`。
- shceme: 请求方式 `默认HTTP`。
- hsot: 连接地址 默认`127.0.0.1`。
- username: 账号。
- password: 密码。
- port: 端口 默认`7474`。

neo4j.pool
- class 连接池驱动类 一般不用改，如果需要可以继承写成自己的 Pool 类也是可以的。
- client 指定当前连接使用的`client`。
- minActive 连接池需要维持的连接数。
- maxActive 连接池最大保持的连接数。
- maxWait 连接池最多等待连接数, 如果没有限制为0(默认)。
- maxWaitTime 连接最大等待时间，单位秒，如果没有限制为0(默认)。
- maxIdleTime 连接最大空闲时间，单位秒。

#### 集群方式
```php
'neo4j'      => [
   'class'   =>\Wunder\Neo4j\Client::class,
   'scheme'  => "http",             
   'hosts'    => [
        [
           "scheme"=>"http",          
           "host"=>"192.168.1.100",   
           "port"=>"7474",
           "username"=>"neo4j",
        ],
        [
           "scheme"=>"http",          
           "host"=>"192.168.1.200",   
           "port"=>"7474",
           "username"=>"neo4j",
        ]
    ]   
],
'neo4j.pool' => [
    'class'  => \Wunder\Neo4j\Pool::class,
    'client' => bean('neo4j'),
    'minActive'   => 10,
    'maxActive'   => 1000,
    'maxWait'     => 0,
    'maxWaitTime' => 5,
    'maxIdleTime' => 40,
]
```
- hsots: 多个节点配置。`ps: host为必填，其他选填，默认获取单机的配置信息`


#### 创建

#### 查询
