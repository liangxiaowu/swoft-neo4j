<?php


namespace Wunder\Neo4j;

use Swoft\Connection\Pool\AbstractPool;
use Wunder\Neo4j\Client;
use Wunder\Neo4j\Connection\Connection;
use Wunder\Neo4j\Connection\ConnectionManager;
use Swoft\Connection\Pool\Contract\ConnectionInterface;
/**
 * Class Pool
 * @package Wunder\Neo4j
 */
class Pool extends AbstractPool
{

    const DEFAULT_POOL = 'neo4j.pool';

    /**
     * @var Client
     */
    private $client;

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * createConnection
     *
     * @return ConnectionInterface
     */
    public function createConnection(): ConnectionInterface
    {
        $id = $this->getConnectionId();

        /** @var Connection $connection */
        $connection  = bean(Connection::class);
        $connection->setId($id);
        $connection->setPool($this);
        $connection->setLastTime();
        $connection->setClient($this->client);
        $connection->create();
        return $connection;
    }

}
