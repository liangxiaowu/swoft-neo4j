<?php


namespace Wunder\Neo4j;

use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * @Bean()
 * Class Client
 * @package Wunder\Neo4j
 */
class Client
{
    // ('http', 'http://neo4j:neo4j@neo4j@192.168.18.150:7474') // Example for HTTP connection configuration (port is optional)
    private $scheme = "http";

    /**
     * Stand alone use
     * @var string
     */
    private $host = "";

    /**
     * @var string
     */
    private $port = "7474";

    /**
     * @var string
     */
    private $username = "neo4j";

    /**
     * @var string
     */
    private $password = "";

    /**
     * Cluster use
     * Parameter does not exist. Use the above default values
     * [
     *  [
     *      "scheme"=>"http"
     *      "host"=>"127.0.0.1"
     *      "port"=>"7474",
     *      "username"=>"neo4j",
     *      "password"=>"123456"
     *  ]
     * ]
     * @var array
     */
    private $hosts = [];

    /**
     * @return string
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     */
    public function setScheme(string $scheme): void
    {
        $this->scheme = $scheme;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port): void
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }


    /**
     * @return array
     */
    public function getHosts(): array
    {
        return $this->hosts;
    }

    /**
     * @param array $hosts
     */
    public function setHosts(array $hosts): void
    {
        $this->hosts = $hosts;
    }



}
