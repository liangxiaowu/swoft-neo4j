<?php


namespace Wunder\Neo4j\Connection;

use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Bean\BeanFactory;
use Swoft\Connection\Pool\AbstractConnection;
use Swoft\Connection\Pool\Contract\PoolInterface;
use Wunder\Neo4j\Builder\ClientAdaptation;
use Wunder\Neo4j\Client;
use Swoft\Bean\Annotation\Mapping\Bean;
use Wunder\Neo4j\Exception\Neo4jException;
use Wunder\Neo4j\Handler\Collection;

/**
 * Class Connection
 *
 * @Bean(scope=Bean::PROTOTYPE)
 *
 * @package Wunder\Neo4j\Connection
 */
class Connection extends AbstractConnection
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @Inject()
     * @var ClientAdaptation
     */
    public $builder;

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param int $lastTime
     */
    public function setLastTime(int $lastTime = null): void
    {
        if (is_null($lastTime)) {
            $lastTime = time();
        }
        $this->lastTime = $lastTime;
    }

    /**
     * @param PoolInterface $pool
     */
    public function setPool(PoolInterface $pool): void
    {
        $this->pool = $pool;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    public function create():void
    {
        $client = \Wunder\Neo4j\Builder\ClientBuilder::create();
        $hosts = $this->client->getHosts();
        $host = $this->client->getHost();
        $port = $this->client->getPort();
        $scheme = $this->client->getScheme();
        $username = $this->client->getUsername();
        $passaword = $this->client->getPassword();
        if (!empty($hosts) && is_array($hosts)){
            foreach ($hosts as $value){
                if(empty($value["host"])){
                    throw new Neo4jException("Host does not exist");
                }
                $host = $value["host"];
                $passaword = isset($value["password"])?$value["password"]:$host;
                $username = isset($value["username"])?$value["username"]:$host;
                $scheme = isset($value["scheme"])?$value["scheme"]:$host;
                $port = isset($value["port"])?$value["port"]:$host;
                $uri = "{$scheme}://{$username}:{$passaword}@{$host}:{$port}";
                $client->addConnection($scheme, $uri);
            }
        }else{
            $uri = "{$scheme}://{$username}:{$passaword}@{$host}:{$port}";
            $client->addConnection($scheme, $uri);
        }

        $this->builder->setClent($client->build());
    }

    /**
     * close
     */
    public function close(): void
    {
    }

    /**
     * reconnect
     *
     * @return bool
     */
    public function reconnect(): bool
    {
    }

    /**
     * release
     *
     * @param bool $force
     */
    public function release(bool $force = false): void
    {
        /* @var ConnectionManager $conManager */
        $conManager = BeanFactory::getBean(ConnectionManager::class);
        $conManager->releaseConnection($this->id);
        parent::release($force);
    }
}
