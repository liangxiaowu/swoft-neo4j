<?php

namespace Wunder\Neo4j\Connection;

use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class BeanTest
 * @package Wunder\Neo4j\Connection
 * @Bean()
 */
class BeanTest
{

    public function getId()
    {
        return "12";
    }

}
