<?php


namespace Wunder\Neo4j\Builder;

use Wunder\Neo4j\Builder\ClientInterface;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * @Bean()
 * Class ClientAdaptation
 * @package Wunder\Neo4j\Builder
 */
class ClientAdaptation
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function setClent($client)
    {
        $this->client = $client;
    }

    public function adaptation($name, $arguments)
    {
        return $this->client->$name(...$arguments);
    }

}
